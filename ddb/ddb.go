package ddb

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodbstreams"
)

type DDBTable struct {
    Name   string
	Stream *DDBStream
	client *dynamodb.DynamoDB
	file   string
}
type DDBStream struct {
	Arn    string
    Shards map[string]*DDBShard
	client *dynamodbstreams.DynamoDBStreams
    rowC   chan map[string]string
}
type DDBShard struct {
    stream  *DDBStream
	ShardId string
    ShardIt string
    SeqNum  string
    SeqType string
}
var SeqTypes = struct {
    First  string
    AtSeq  string
    AftSeq string
    Latest string
}{ "TRIM_HORIZON", "AT_SEQUENCE_NUMBER", "AFTER_SEQUENCE_NUMBER", "LATEST"}

// There are 0, 1, or 2 state files:
// - the data from DDB (all the records)
// - the DDB state (shards/iterators and sequence numbers)
//
// Here are the combinations, and how we proceed:
// - data state file
// 1. load - there is no ddb state file, so initialize ddb state from scratch
// 2. mark - gets current last sequence number(s) on shard(s)
// 3. read - discards all that data and re-reads table from scratch
// 4. pull - start pulling all records from stream based on marked ddb state
//
// - ddb state file
// 1. load - loads in the previously saved ddb state from the JSON ddb state file
// 2. mark - gets current last sequence number(s) on shard(s)
// 3. read - reads table from scratch
// 4. pull - start pulling all records from stream based on marked ddb state (last sequence numbers)
//
// - data and ddb state files (so easy!!)
// 1. load - loads in the previously saved ddb state from the JSON ddb state file
// 2. pull - start pulling all records from stream based on saved ddb state (last sequence numbers)
// 
// - neither state file
// 1. load - there is no ddb state file, so initialize from scratch
// 2. mark - gets current last sequence number(s) on shard(s)
// 3. read - discards all that data and re-reads table from scratch
// 4. pull - start pulling all records from stream based on marked ddb state
//
// Note: without both data and ddb state files, must start over from scratch! :(
func (table *DDBTable) init(sess *session.Session, baseN, name string, loadDDB bool) []map[string]string {
	table.file = fmt.Sprintf("%s.ddb", baseN)
	ddbState  := false
    log("DDB init: starting: loadDDB=%v, file=%s name=%s", loadDDB, table.file, name)
    defer log("DDB init: complete")
    if data, err := ioutil.ReadFile(table.file); err == nil {
        if err := json.Unmarshal(data, table); err == nil {
            ddbState = true
        }
    }
    if !ddbState {
        table.Name   = name
        table.Stream = &DDBStream{}
        table.Stream.Shards = map[string]*DDBShard{}
		log("DDB init: no local DDB state found, will need to pull all AWS DDB data and reset stream partitions/offsets")
    } else {
		log("DDB init: found local DDB state, if we have local data, then will not pull AWS DDB data.")
	}
    if table.Name == "" {
        table.Name = name
    }
	// These items do not marshal/unmarshal with the JSON file, so whether the JSON file was found or not, we must initialize these in-memory-only objects.
    table.client        = dynamodb.New(sess)
    table.Stream.client = dynamodbstreams.New(sess)
	table.Stream.rowC   = make(chan map[string]string)
    for _, shard := range table.Stream.Shards {
        shard.stream = table.Stream
    }
	log("DDB init: getting stream ARN (may block/time-out if credentials invalid)")
    dti := &dynamodb.DescribeTableInput{TableName: aws.String(name)}
    if dso, err := table.client.DescribeTable(dti); err == nil {
		if dso.Table.LatestStreamArn != nil {
			table.Stream.Arn = *dso.Table.LatestStreamArn
		} else {
			panic(fmt.Sprintf("DDB init: stream not enabled on table [%s]", name))
		}
    } else {
		panic(fmt.Sprintf("DDB init: cannot get ARN for table [%s]: %s", name, err.Error()))
	}
	log("DDB init: obtained stream ARN [%s]", table.Stream.Arn)

    table.Stream.syncShards()

	if ddbState && !loadDDB {
		return []map[string]string{}
	} else {
		table.mark()
		return table.read()
	}
}

func (table *DDBTable) save() {
	log("DDB save: saving DDB state to local cache")
    if data, err := json.Marshal(table); err == nil {
        if err := ioutil.WriteFile(table.file, data, 0644); err != nil {
            log("DDB save: failed to save JSON DDB state: %v", err.Error())
        } else {
            log("DDB save: saved DDB state to local cache")
        }
    } else {
        log("DDB save: failed to marshal DDB state to JSON: %v", err.Error())
    }
}

func (table *DDBTable) mark() {
    log("DDB mark: table=%s - starting marking of shards", table.Name)
    //defer log("DDB mark: table=%s - done", table.Name)
    for _, shard := range table.Stream.Shards {
        shard.mark()
    }
}

func (table *DDBTable) read() []map[string]string {
    // This reads the full DDB table from scratch. 
    all   := []map[string]string{}
    si    := &dynamodb.ScanInput{TableName: aws.String(table.Name)}
    retry := 500
    curr  := 0
    total := 0
    for {
        if so, err := table.client.Scan(si); err == nil {
            curr  += len(so.Items)
            total += len(so.Items)
            if curr >= 50000 {
                log("DDB read: %d read", total)
                curr = 0
            }
            all = append(all, table.attrsToRows(so.Items)...)  // Could be no items. That's okay.
            if len(so.LastEvaluatedKey) == 0 {
                // Doc says this means nothing more to read.
                log("DDB read: complete, total %d", total)
                return all
            }
            si.ExclusiveStartKey = so.LastEvaluatedKey
        } else {
            if aerr, ok := err.(awserr.Error); ok {
                switch aerr.Code() {
                case dynamodb.ErrCodeProvisionedThroughputExceededException:
                    fallthrough
                case dynamodb.ErrCodeLimitExceededException:
                    time.Sleep(time.Duration(retry)*time.Millisecond)
                    retry *= 2
                    if retry >= 30*1000 {
						retry = 30*1000
					}
                case dynamodb.ErrCodeInternalServerError:
                    time.Sleep(time.Duration(1000*time.Millisecond))

                case dynamodb.ErrCodeResourceNotFoundException:
                    fallthrough
                default:
                    return all
                }
            }
        }
    }
}

func (table *DDBTable) run(stop chan interface{}, done chan bool) {
    interval := int64(0)
    for {
        select {
        case <-time.After(time.Duration(interval)*time.Second):
            recs := table.pull()
            if len(recs) > 0 {
                for _, rec := range recs {
                    table.Stream.rowC <- rec
                }
                interval = 0
            } else {
                interval = 5
            }
        case <-stop:
			stop = nil
			log("DDB run: stop event received")
            done <-true
			log("DDB run: worker exiting")
            return
        }
    }
}

func (table *DDBTable) pull() []map[string]string {
    all := []map[string]string{}
    for _, shard := range table.Stream.Shards {
        recs := shard.pull()
        all = append(all, recs...)
    }
    return all
}

func (strm *DDBStream) syncShards() {
    log("DDB sync: starting: arn=%s, shards=%d", strm.Arn, len(strm.Shards))
    //defer log("DDB sync: completed: arn=%s", strm.Arn)
	dsi := &dynamodbstreams.DescribeStreamInput{
		StreamArn: aws.String(strm.Arn),
	}
    
    if dso, err := strm.client.DescribeStream(dsi); err == nil {
        desc := dso.StreamDescription
        endShards := make(map[string]interface{})

        // Move the AWS shards into a map so we can find them more easily.
        awsShards := make(map[string]*dynamodbstreams.Shard)
        for _, awsShard := range desc.Shards {
            awsShards[*awsShard.ShardId] = awsShard
        }
		
        // Go through our own shards and match them up to the shards in AWS.
		lcl := "     "
		aWS := "   "
		ms1 := "closed remotely, read completely, removing locally  "
		//ms2 := "closed remotely, read partially, will keep reading  "
		ms7 := "closed remotely, not yet started, will start reading"
		//ms3 := "open remotely, read partially, will keep reading    "
		ms4 := "open remotely, not yet started, will start reading  "
		ms5 := "not found remotely, removing shard locally          "
		ms6 := "found remotely, not yet in local, creating locally  "
        for _, ourShard := range strm.Shards {
			osn := ourShard.SeqNum
			lcl = "local"
            // Look at each shard in AWS
            if awsShard, ok := awsShards[ourShard.ShardId]; ok {
				aWS = "AWS"
                // Is this AWS shard closed (has an ending sequence number)?
                ssn := ss(awsShard.SequenceNumberRange.StartingSequenceNumber)
                esn := ss(awsShard.SequenceNumberRange.EndingSequenceNumber)
                if esn != "" {
                    // We have an ending sequence number, so this shard is finished in AWS.
                    if ourShard.SeqNum == esn {
                        // Our shard sequence number matches the last AWS sequence number, so our shard is done (no more data to read)
						log("DDB sync: shard=%s (%s/%s) %s - seq[%s] ssn[%s] esn[%s]", ourShard.name(), lcl, aWS, ms1, osn, ssn, esn)
                        endShards[ourShard.ShardId] = nil  // Add the shardId to this deletion map. We'll use it to delete from our shard map.
                    } else {
						if osn != "" {
							//log("DDB sync: shard=%s (%s/%s) %s - seq[%s] ssn[%s] esn[%s]", ourShard.name(), lcl, aWS, ms2, osn, ssn, esn)
						} else {
							log("DDB sync: shard=%s (%s/%s) %s - seq[%s] ssn[%s] esn[%s]", ourShard.name(), lcl, aWS, ms7, osn, ssn, esn)
						}
						ourShard.getIterator()
					}
                } else {
					if osn != "" {
						//log("DDB sync: shard=%s (%s/%s) %s - seq[%s] ssn[%s] esn[%s]", ourShard.name(), lcl, aWS, ms3, osn, ssn, esn)
					} else {
						log("DDB sync: shard=%s (%s/%s) %s - seq[%s] ssn[%s] esn[%s]", ourShard.name(), lcl, aWS, ms4, osn, ssn, esn)
					}
                    // The AWS shard is still open, get a fresh iterator - just in case it had expired
                    ourShard.getIterator()
                }
            } else {
                // Our shard is not in AWS
				log("DDB sync: shard=%s (%s/%s) %s - seq[%s]", ourShard.name(), lcl, aWS, ms5, osn)
                endShards[ourShard.ShardId] = nil  // Add the shardId to this deletion map. We'll use it to delete from our shard map.
            }
        }
        // Find the shards that are not local (in AWS only), and add them locally
        for awsShardId, awsShard := range awsShards {
			aWS = "AWS"
            // Is this AWS shard closed (has an ending sequence number)? We only want open shards
            ssn := ss(awsShard.SequenceNumberRange.StartingSequenceNumber)
            esn := ss(awsShard.SequenceNumberRange.EndingSequenceNumber)
            if esn == "" {
                if _, ok := strm.Shards[awsShardId]; !ok {
                    strm.Shards[awsShardId] = &DDBShard{
                        stream:  strm,
                    	ShardId: awsShardId,
                    	SeqType: SeqTypes.First,
                    }
                    strm.Shards[awsShardId].getIterator()
					log("DDB sync: shard=%s (%s/%s) %s - seq[%s] ssn[%s] esn[%s]", strm.Shards[awsShardId].name(), lcl, aWS, ms6, "", ssn, esn)
                }
            }
        }
        // Delete the invalid (done) shards from our local map.
        for shardId := range endShards {
            delete(strm.Shards, shardId)
        }
    } else {
        // TODO: stream no longer valid
        log("DDB sync: stream no longer valid, describe failed: %s", err.Error())
    }
	for _, shard := range strm.Shards {
		log("DDB sync: completed: shard=%s seq[%s] type[%s]", shard.name(), shard.SeqNum, shard.SeqType)
	}
}

func (shard *DDBShard) getIterator() error {
    gsii := &dynamodbstreams.GetShardIteratorInput{
    	ShardId:           aws.String(shard.ShardId),
    	ShardIteratorType: aws.String(shard.SeqType),
    	StreamArn:         aws.String(shard.stream.Arn),
    }
    if shard.SeqNum != "" {
        gsii.SequenceNumber = aws.String(shard.SeqNum)
    }
    shard.ShardIt = ""
    //log("DDB iter: shard=%s - starting", shard.name())
    //defer log("DDB iter: shard=%s - done", shard.name())
    for {
        if si, err := shard.stream.client.GetShardIterator(gsii); err == nil {
            log("DDB iter: shard=%s - new iterator returned: seq[%s] type[%s]", shard.name(), shard.SeqNum, shard.SeqType)
            shard.ShardIt = *si.ShardIterator
            return nil
        } else {
			log("DDB iter: shard=%s - error: seq[%s] type[%s]: %s", shard.name(), shard.SeqNum, shard.SeqType, err.Error())
            if aerr, ok := err.(awserr.Error); ok {
                switch aerr.Code() {
                case dynamodbstreams.ErrCodeTrimmedDataAccessException:
                    shard.SeqType = SeqTypes.First   // Okay, we're skipping missed events - going back to the oldest - best we can do.
					shard.SeqNum  = ""
                    gsii.ShardIteratorType = aws.String(shard.SeqType)
                    gsii.SequenceNumber    = nil
                    
                case dynamodbstreams.ErrCodeInternalServerError:
                    time.Sleep(time.Duration(2)*time.Second)
                    continue
    
                default:
                    return err
                }
            } else {
                return err
            }
        }
    }
}

func (shard *DDBShard) mark() {
    if shard.ShardIt == "" {
        panic("shard has no iterator")
    }
    size := int64(1)
    retry := 500
    for {
		gri  := &dynamodbstreams.GetRecordsInput{
			Limit:         aws.Int64(size),
			ShardIterator: aws.String(shard.ShardIt),
		}
        if gro, err := shard.stream.client.GetRecords(gri); err == nil {
            for _, record := range gro.Records {
                shard.SeqNum  = *record.Dynamodb.SequenceNumber
                shard.SeqType = SeqTypes.AtSeq
                log("DDB mark: shard=%s seq[%s] - record returned", shard.name(), shard.SeqNum)
                shard.getIterator() // Resets a new iterator to point to the sequence number of this message (so that next time we pull, we get this message!)
                return
            }
			// If we just tried SeqTypes.First, that was our final try!
			if shard.SeqType != SeqTypes.First {
				log("DDB mark: shard=%s - resetting to TRIM_HORIZON and trying again (records may be missed)", shard.name())
				shard.SeqNum  = ""
				shard.SeqType = SeqTypes.First	// We could set to Last instead of First, but First will go back to beginning of stream, which gives us most data.
				shard.getIterator()				// New iterator based on SeqTypes.First
            	gri.ShardIterator = aws.String(shard.ShardIt)
				continue
			}
			log("DDB mark: shard=%s - no record from iterator", shard.name())
            return
        } else {
            log("DDB mark: shard=%s - error: %s", shard.name(), err.Error())
            // Most likely throttling. Need to sleep and retry.
            if aerr, ok := err.(awserr.Error); ok {
				switch aerr.Code() {
                case dynamodbstreams.ErrCodeInternalServerError:
                    fallthrough
				case dynamodbstreams.ErrCodeLimitExceededException:
					if retry >= 30*1000 {
						retry = 30*1000
					}
                    log("DDB mark: shard=%s throttle: %d: %s", shard.name(), retry, err.Error())
					time.Sleep(time.Duration(retry) * time.Millisecond)
					retry *= 2

                case dynamodbstreams.ErrCodeExpiredIteratorException:
                    fallthrough
                case dynamodbstreams.ErrCodeTrimmedDataAccessException:
                    shard.getIterator()

                case dynamodb.ErrCodeResourceNotFoundException:
                    fallthrough
				default:
                    shard.stream.syncShards()
				}
			} else {
                shard.stream.syncShards()
			}
        }
    }
}

func (shard *DDBShard) pull() []map[string]string {
    if shard.ShardIt == "" {
		shard.getIterator()
		if shard.ShardIt == "" {
			panic("shard has no iterator")
		}
    }
    rows := make([]map[string]string, 0)
    size := int64(1000)
    gri  := &dynamodbstreams.GetRecordsInput{
    	Limit:         aws.Int64(size),
    	ShardIterator: aws.String(shard.ShardIt),
    }
    retry := 500
    for {
        if gro, err := shard.stream.client.GetRecords(gri); err == nil {
            //log("DDB pull: shard=%s - %d records", shard.name(), len(gro.Records))
            for _, record := range gro.Records {
                shard.SeqNum  = *record.Dynamodb.SequenceNumber
                shard.SeqType = SeqTypes.AftSeq
                
                var avs map[string]*dynamodb.AttributeValue
                row := make(map[string]string)
                row["_op"] = *record.EventName
                if *record.EventName == "INSERT" || *record.EventName == "MODIFY" {
                    avs = record.Dynamodb.NewImage
                } else {
                    avs = record.Dynamodb.OldImage
                }
                rows = append(rows, attrsToRow(avs))
            }
            if gro.NextShardIterator != nil && *gro.NextShardIterator != "" {
                shard.ShardIt = *gro.NextShardIterator
            }
        } else {
            // Most likely throttling. Need to sleep and retry.
            if aerr, ok := err.(awserr.Error); ok {
				switch aerr.Code() {
                case dynamodbstreams.ErrCodeInternalServerError:
				case dynamodbstreams.ErrCodeLimitExceededException:
					if retry >= 30*1000 {
						retry = 30*1000
					}
                    log("DDB pull: shard=%s - throttle: %d: %s", shard.name(), retry, err.Error())
					time.Sleep(time.Duration(retry) * time.Millisecond)
					retry *= 2

                case dynamodbstreams.ErrCodeExpiredIteratorException:
                    fallthrough
                case dynamodbstreams.ErrCodeTrimmedDataAccessException:
                    log("DDB pull: shard=%s - error: %s", shard.name(), err.Error())
                    shard.getIterator()

                case dynamodb.ErrCodeResourceNotFoundException:
                    fallthrough
				default:
					log("DDB pull: shard=%s - invalid (%T): %s", shard.name(), aerr, err.Error())
                    shard.stream.syncShards()
				}
			} else {
				log("DDB pull: shard=%s - invalid: %s", shard.name(), err.Error())
                shard.stream.syncShards()
			}
        }
        return rows
    }
}

func (shard *DDBShard) name() string {
	parts := strings.Split(shard.ShardId, "-")
	return parts[len(parts)-1]
}

func (table *DDBTable) write(name string, fields map[string]string) error {
	items := map[string]*dynamodb.AttributeValue{}
	for key, val := range fields {
		items[key] = &dynamodb.AttributeValue{ S: aws.String(val) }
	}
	pii := &dynamodb.PutItemInput{ Item: items, TableName: aws.String(name) }

	var cancel func()
	timeout := time.Duration(10)*time.Second
	contxt  := context.Background()
	contxt, cancel = context.WithTimeout(contxt, timeout)
	defer cancel()

	for {
		// If cannot reach DDB, this call blocks for 10 seconds.
		if _, err := table.client.PutItemWithContext(contxt, pii); err != nil {
			if contxt.Err() != nil {
				return contxt.Err()
			}
			//log("DDB fail: %v", err.Error())
			if aerr, ok := err.(awserr.Error); ok {
				switch aerr.Code() {
				case dynamodb.ErrCodeLimitExceededException:
					fallthrough
				case dynamodb.ErrCodeProvisionedThroughputExceededException:
					//log("DDB backoff: %v", aerr.Message())
					time.Sleep(time.Duration(500)*time.Millisecond)
					continue
				default:
					return err
				}
			}
		} else {
			if contxt.Err() != nil {
				return contxt.Err()
			}
			return nil
		}
	}
}

func (table *DDBTable) attrsToRows(items []map[string]*dynamodb.AttributeValue) []map[string]string {
    rows := make([]map[string]string, 0, len(items))
    for _, item := range items {
        rows = append(rows, attrsToRow(item))
    }
    return rows
}

func attrsToRow(item map[string]*dynamodb.AttributeValue) map[string]string {
	row := make(map[string]string)
    for attrName, attrVal := range item {
		if attrVal.S != nil {
			row[attrName] = *attrVal.S
		} else if attrVal.N != nil {
			row[attrName] = *attrVal.N
		}
    }
    return row
}

func ss(str *string) string {
    if str != nil {
        return *str
    }
    return ""
}

func log(str string, args ...interface{}) {

}